# Copyright (c) 2016, Novatron Oy
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import numpy as np
import pyproj
from numpy import matlib as ML
import math
import sys

def solve_affine_2d(source, expected):
    """
    Calculates the 2d affine transformation parameters. Returns a tuple

        (rotation, scale, dn, de)

    where rotation is in radians. This function restricts the solving so
    that only one scale value (which is used for both axes) is produced.

    Input matrices should be formatted as

    [[northing easting height],
     [northing easting height],
     ...]

    http://stackoverflow.com/a/14271196/391350
    """

    # The affine transformation matrix is like this:
    #
    #         |s*cos(a)  -s*sin(a)  dn|
    #     A = |s*sin(a)   s*cos(a)  de|
    #         |0          0          1|
    #
    # Solve the equation
    #
    #     |ne0|      |ns0  es0 1 0|   |A00|
    #     |ee0|      |es0 -ns0 0 1|   |A01|
    #     |ne1|    = |ns1  es1 1 0| * |A02|
    #     |ee1|      |es1 -ns1 0 1|   |A12|
    #     |...|      |...         |
    #
    #     expected = source         * M
    #
    #     M = pinv(source) * expected

    X = ML.zeros((expected.shape[0]*2, 1))

    for index, row in enumerate(expected):
        X[index*2, 0] = row[0, 0]
        X[index*2+1, 0] = row[0, 1]

    Y = ML.zeros((expected.shape[0]*2, 4))

    for index, row in enumerate(source):
        Y[index*2, 0] = row[0, 0]
        Y[index*2, 1] = row[0, 1]
        Y[index*2, 2] = 1
        Y[index*2, 3] = 0

        Y[index*2+1, 0] = row[0, 1]
        Y[index*2+1, 1] = -row[0, 0]
        Y[index*2+1, 2] = 0
        Y[index*2+1, 3] = 1

    trans = np.dot(np.linalg.pinv(Y), X)

    # Now `A` can be constructed from `trans` as follows:
    #
    #     A[0, 0] = trans[0, 0]       (s*cos(a))
    #     A[0, 1] = trans[1, 0]       (-s*sin(a))
    #     A[0, 2] = trans[2, 0]       (dn)
    #     A[1, 2] = trans[3, 0]       (de)
    #
    # The rest of A can be derived from trans as follows:
    #
    #     A[1, 0] = -trans[1, 0]      (s*sin(a))
    #     A[1, 1] = trans[0, 0]       (s*cos(a))
    #
    # To get scale and rotation from `A`, we can do this:
    #
    #     s * cos(a) = Q     => s = Q / cos(a)
    #
    #     s * sin(a) = W
    #     (Q / cos(a)) * sin(a) = W
    #     Q * sin(a) / cos(a) = W
    #     Q * tan(a) = W
    #     tan(a) = W / Q
    #     a = atan(W / Q)
    #
    # Where `Q` and `W` are `A[0, 0]` and `A[1, 0]` respectively.

    s_cos, s_sin, dn, de = trans[0,0], trans[1,0], trans[2,0], trans[3,0]

    rotation = math.atan2(-s_sin, s_cos)
    scale = s_cos / math.cos(rotation)

    return (rotation, scale, dn, de)

def helmert2d(coordinates, dn, de, rotation, scale):
    """
    Basically an affine transformation with uniform scale.

    Input:

    [[n e h],
     [n e h],
     [n e h],
     ...]
    """
    a = scale * math.cos(rotation)
    b = scale * math.sin(rotation)

    result = ML.zeros(coordinates.shape)

    for index, row in enumerate(coordinates):
        n = dn + a*row[0, 0] - b*row[0, 1]
        e = de + a*row[0, 1] + b*row[0, 0]

        result[index] = [n, e, row[0, 2]]

    return result

def projection(coordinates):
    """
    Project the input WGS84 coordinates to a plane. Uses transverse mercator
    projection with prime meridian and central latitude calculated from the
    center of coordinates.

    Input data should be of the form:

    [[lat lon alt],
     [lat lon alt],
     [lat lon alt],
     ...]
    """
    mean = coordinates.mean(axis=0)

    prime_meridian = mean[0, 1]
    central_latitude = mean[0, 0]

    pj_wgs84 = pyproj.Proj('+proj=longlat +datum=WGS84 +units=m +no_defs')
    pj_proj = pyproj.Proj('+proj=tmerc +ellps=GRS80 +towg84=0,0,0 +lon_0={} +lat_0={} +x_0=0 +y_0=0 +k_0=1 +units=m +no_defs'.format(prime_meridian, central_latitude))

    result = ML.zeros(coordinates.shape)

    for index, row in enumerate(coordinates):
        n, e, u = row.tolist()[0]

        # notice the parameter order is enu
        e, n, u = pyproj.transform(pj_wgs84, pj_proj, e, n, u)

        result[index] = [n, e, u]

    return result

def fit_plane(X):
    """
    Calculate the best fitting plane from 3D point data using singular value
    decomposition. Returns the normal of the plane and a point which lies
    on the plane.

    Input data as follows:

    [[northing easting height],
     [northing easting height],
     ...]

    http://math.stackexchange.com/questions/99299/best-fitting-plane-given-a-set-of-points
    http://mathforum.org/library/drmath/view/63765.html
    http://math.stackexchange.com/questions/3869/what-is-the-intuitive-relationship-between-svd-and-pca/3871#3871
    http://www.mathworks.com/matlabcentral/fileexchange/43305-plane-fit/content/affine_fit.m
    """

    # Row-wise mean (i.e. centroid)
    p = X.mean(axis=0)

    # Reduce samples (subtract centroid)
    R = X - p

    U, s, V = np.linalg.svd(R, full_matrices=False)

    # Normal vector of the best fitting plane is the singular vector
    # corresponding to the least singular value. The values are in
    # descending order so take the last vector.
    n = V.T[:,-1]

    return (n, p)

def calculate_plane_params(n, p):
    """
    Calculate the plane parameters from the normal of the plane and a point p
    that lies on the plane.

    Returns A, B and C which you can use like this:

        z = A*x + B*y + C
    """

    a, b, c = n[0,0], n[1,0], n[2,0]

    d = -(a*p[0,0] + b*p[0,1] + c*p[0,2])

    A = a / c
    B = b / c
    C = d / c

    # We could return a, b, c, d but we simplify a bit. Here is the equation
    # for a plane:
    #
    #     ax + by + cz + d = 0
    #     z = -(ax + by + d) / c
    #     z = -(a/c*x + b/c*y + d/c)
    #
    # We divide the constants inside the parentheses by c and after that we
    # are left with only 3 constants, which we return from the function.

    return (A, B, C)

def calculate_correction_plane(source, expected):
    """
    Calculates parameters for a correction plane that maps heights from source
    to expected. To calculate a corrected height using the returned parameters,
    you do the following:

        corrected = original + (A*x + B*y + C)
    """

    # Feed expected to fit_plane with a little modification: the elevation
    # values should be the difference of source and expected elevations.
    diff = expected.copy()
    diff[:,-1] = source[:,-1] - expected[:,-1]

    n, p = fit_plane(diff)

    return calculate_plane_params(n, p)

def vertical_correction(coordinates, params):
    A, B, C = params

    result = coordinates.copy()

    for index, row in enumerate(coordinates):
        x, y, z = row[0,0], row[0,1], row[0,2]
        result[index, 2] = z + (A*x + B*y + C)

    return result

if __name__ == '__main__':
    filename = 'data.txt'

    if len(sys.argv) == 2:
        filename = sys.argv[1]

    data = np.loadtxt(filename)

    # First three columns
    source = np.matrix(data[:, 0:3])

    # Last three columns
    expected = np.matrix(data[:, 3:6])

    # Project the WGS84 coordinates to a plane
    projected = projection(source)

    print('projected source coordinates\n', projected)

    # Calculate the parameters for the local transformation
    rotation, scale, dn, de = solve_affine_2d(projected, expected)

    print('\naffine parameters')
    print('  rotation: {} degrees'.format(rotation * 180 / math.pi))
    print('  scale: {}'.format(scale))
    print('  dn: {}'.format(dn))
    print('  de: {}'.format(de))

    # Calculate correction parameters for the height
    vcorrection = calculate_correction_plane(source, expected)

    print('\nheight correction plane')
    print('  A: {}\n  B: {}\n  C: {}'.format(*vcorrection))

    # Perform the 2d local transformation
    transformed = helmert2d(projected, dn, de, rotation, scale)

    print('\ntransformed coordinates\n', transformed)

    # Perform height correction
    vcorrected = vertical_correction(transformed, vcorrection)

    print('\nheight corrected coordinates\n', vcorrected)

    # Calculate the difference of expected and actual coordinates
    diff = expected - vcorrected

    # Result contains the error for plane and for height
    result = ML.zeros((diff.shape[0], 2))

    for index, row in enumerate(diff):
        result[index, 0] = math.sqrt(row[0,0]**2 + row[0,1]**2)
        result[index, 1] = diff[index, 2]

    print('\nerrors (plane and height)\n', result)
