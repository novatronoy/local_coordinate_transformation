# Local coordinate transformation

The code in this repository can be used to calculate parameters for a coordinate
transformation from WGS84 coordinates to local grid coordinates, when you have
some common points in both systems.

## Calculating the transformation parameters

1. project the geographic coordinates to a plane using transverse mercator
   projection and using the center of the coordinates as prime meridian and
   central latitude.

2. calculate the 2D affine transformation parameters (rotation, scale,
   translations) for transforming from the projected coordinates to the target
   coordinates

3. calculate correction plane parameters from the height differences

## Using the parameters

1. project source coordinates as in step 1 of previous section

2. perform the 2d affine transformation (this affects only the northing and
   easting values)

3. perform the height correction

## Trying out with provided sample data

First you should install the requirements (numpy, pyproj):

    $ python3 -mvenv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt

And then just run the script. The test data is by default read from `data.txt`:

    $ python local_coordinate_transformation.py

You can specify the file from which the script reads data:

    $ python local_coordinate_transformation.py some_other_data_file.txt

The script outputs the results from most of the stages for easy debugging.

# License

2-clause BSD. The license text can be found in the COPYING file.

# Contact us

If you've found this code helpful in any way, we would appreciate it if you
found the time to drop us a line at <rnd@novatron.fi>.
